import sbt.util

shellPrompt in ThisBuild := { state => Project.extract(state).currentRef.project + "> " }

logLevel := util.Level.Info

val allResolvers = Seq(
  Resolver.sonatypeRepo("public"),
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)


lazy val root = (project in file("."))
  .settings(
    scalaVersion                  := "2.13.1",
    name                          := "WebsocketsPoc",
    organization                  := "no.trainor.magnus",
    parallelExecution             := false,
    parallelExecution in Test     := false,
    libraryDependencies :=  Seq(
      "org.slf4j"                     % "slf4j-api"                     % "1.7.25",
      "org.codehaus.janino"           % "janino"                        % "3.0.8",
      "ch.qos.logback"                % "logback-classic"               % "1.2.3",
      "io.circe"               %% "circe-generic"             % "0.12.3",
      "io.circe"               %% "circe-parser"              % "0.12.3",
      "io.circe"               %% "circe-derivation"          % "0.12.0-M7",
      "joda-time"               % "joda-time"                 % "2.10.5",

      "dev.zio"                %% "zio"                       % "1.0.0-RC17",
      "dev.zio"                %% "zio-interop-cats"          % "2.0.0.0-RC8",
      "org.http4s"             %% "http4s-blaze-server"       % "0.21.0-M5",
      "org.http4s"             %% "http4s-blaze-client"       % "0.21.0-M5",
      "org.http4s"             %% "http4s-dsl"                % "0.21.0-M5",
      "org.http4s"             %% "http4s-circe"              % "0.21.0-M5"
    ),
    resolvers ++= allResolvers,
    packJarNameConvention := "original",
    packDir := "pack",
    packMain := Map(
      "WebsocketsPoc" -> "no.trainor.magnus.websocketspoc"
    )
  ).enablePlugins(PackPlugin)

