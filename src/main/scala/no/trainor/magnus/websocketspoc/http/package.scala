package no.trainor.magnus.websocketspoc


import no.trainor.magnus.websocketspoc.Main.AppTask

import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.slf4j.LoggerFactory
import zio.Task

package object http {
  val dsl: Http4sDsl[AppTask] = Http4sDsl[AppTask]
  import dsl._
  object clientDsl extends Http4sClientDsl[Task]
  import zio.interop.catz._



}
