package no.trainor.magnus
package websocketspoc
package http

import fs2.concurrent.Topic
import no.trainor.magnus.websocketspoc.Main.AppTask
import org.http4s.HttpRoutes
import org.http4s.server.websocket.WebSocketBuilder
import org.http4s.websocket.WebSocketFrame
import org.slf4j.LoggerFactory
import zio.ZIO
import zio.interop.catz._

class WebSocketService(outputTopic: Topic[zio.Task, MockMessage], inputHandler:MockInputHandler) {
  import dsl._
  val logger = LoggerFactory.getLogger("http.ws")

  def service(): HttpRoutes[AppTask] = HttpRoutes.of[AppTask] {
    case req @ GET -> Root / "test" / identifier =>
       val toClient = outputTopic.subscribe(100).filter(_.identifier == identifier).map(msg => WebSocketFrame.Text(msg.message))
       WebSocketBuilder[AppTask].build(
        toClient,
        _.evalMap(in => inputHandler.handleInput(in,identifier)).onFinalize(ZIO.effect{println(">> Inputstream closed")})
      )
  }
}
