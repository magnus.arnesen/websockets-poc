package no.trainor.magnus

import cats.syntax.either._
import fs2.concurrent.Topic
import io.circe.{Decoder, Encoder}
import org.http4s.websocket.WebSocketFrame
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.{Logger, LoggerFactory, MarkerFactory}
import zio.blocking.Blocking
import zio.{Task, UIO, ZIO}

package object websocketspoc {

  implicit class LoggerExtensions(logger: Logger) {

    private def log(f: => Unit): UIO[Unit] = Task.effect(f).either *> UIO.unit

    def traceUIO(message: String): UIO[Unit] = log(logger.trace(message))
    def traceUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.trace(message, throwable))

    def debugUIO(message: String): UIO[Unit] = log(logger.debug(message))
    def debugUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.debug(message, throwable))

    def infoUIO(message: String): UIO[Unit] = log(logger.info(message))
    def infoUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.info(message, throwable))

    def warnUIO(message: String): UIO[Unit] = log(logger.warn(message))
    def warnUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.warn(message, throwable))

    def errorUIO(message: String): UIO[Unit] = log(logger.error(message))
    def errorUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.error(message, throwable))


    private val fatal = MarkerFactory.getMarker("FATAL")

    def fatalUIO(message: String): UIO[Unit] = log(logger.error(fatal, message))
    def fatalUIO(message: String, throwable: Throwable): UIO[Unit] = log(logger.error(fatal, message, throwable))
  }

  def decodeJson[T:Decoder](str:String):Task[T] = {
    import io.circe.parser._
    decode[T](str).fold(error => Task.fail(new Exception("Fail to decode payload: "+str, error)), ok => Task.succeed(ok))
  }

  type BlockingTask[A] = ZIO[Blocking, Throwable, A]

  def currentTime:UIO[DateTime] = UIO.effectTotal(new DateTime)

  implicit val dateTimeEncoder: Encoder[DateTime] = Encoder.encodeString.contramap[DateTime](_.toString(ISODateTimeFormat.dateTime))
  implicit val dateTimeDecoder: Decoder[DateTime] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(ISODateTimeFormat.dateTime.parseDateTime(str)).leftMap(_.getMessage)
  }
  case class MockMessage(identifier:String, message:String)
  class MockInputHandler (outputTopic:Topic[Task, MockMessage]){
    private val logger = LoggerFactory.getLogger("inputhandler")
    def handleInput(frame:WebSocketFrame, identifier:String):UIO[Unit] = {
      frame match {
        case WebSocketFrame.Text(str) => logger.infoUIO(s"Input from identifier $identifier: $str")
        case _ => UIO.unit
      }
    }
  }



}
