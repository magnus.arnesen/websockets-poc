package no.trainor.magnus.websocketspoc

import java.util.concurrent.Executors

import cats.effect.ExitCode
import fs2.concurrent.Topic
import no.trainor.magnus.websocketspoc.http.WebSocketService
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze._
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.{Logger, LoggerFactory}
import zio._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.console.Console
import zio.random.Random
import zio.system.System

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.concurrent.duration._

object Main extends zio.App {
  self =>
  type AppEnvironment = ZEnv
  type AppTask[A] = RIO[ZEnv, A]

  lazy val logger: Logger = LoggerFactory.getLogger("main")

  val httpExecutionContext: ExecutionContextExecutor = ExecutionContext.fromExecutor(Executors.newWorkStealingPool(10))

  import zio.interop.catz._

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {

    val appLogic = for {
      outputTopic <- fs2.concurrent.Topic[Task, MockMessage](MockMessage("INIT", "intitial message"))
      httpApp = Router[AppTask](
        mappings =
          "/ws" -> new WebSocketService(outputTopic, new MockInputHandler(outputTopic)).service()
      ).orNotFound
      mockSender = new MockMessageSender(outputTopic)
      fiber <- mockSender.start
      server = ZIO.runtime[AppEnvironment].flatMap { implicit runtime =>
        val port = 21337
        BlazeServerBuilder[AppTask]
          .bindHttp(port, "0.0.0.0")
          .withHttpApp(httpApp)
          .withConnectorPoolSize(2)
          .withResponseHeaderTimeout(120.second)
          .withIdleTimeout(121.second)
          .serve
          .compile[AppTask, AppTask, ExitCode]
          .drain
      }

      _ <- logger.infoUIO("STARTED start WS client on ws://localhost:21137/ws/test/id<1-4>")
      program <- server.provideSome[ZEnv] { base =>
        new Blocking with Console with Clock with System with Random {
          override val blocking: Blocking.Service[Any] = base.blocking
          override val console: Console.Service[Any] = base.console
          override val clock: Clock.Service[Any] = base.clock
          override val system: System.Service[Any] = base.system
          override val random: Random.Service[Any] = base.random
        }
      }
    } yield program

    appLogic.foldM(
      error => logger.fatalUIO("THIS has encountered an fatal error", error) *> ZIO.succeed(1),
      _ => logger.infoUIO("Shutting down THIS gracefully") *> ZIO.succeed(0)
    )

  }
}

class MockMessageSender(outTopic: Topic[Task, MockMessage]) {

  val logger = LoggerFactory.getLogger("messagesender")
  //This would probably be a
  def start: URIO[Clock, Fiber[Unit, Nothing]] = {
    val prg = for {
      _ <- Task.unit
      id = scala.util.Random.between(1,5)
      msg = MockMessage("id"+id, "This message to identifier id"+id+": "+new DateTime().toString(ISODateTimeFormat.dateTime()))
      //_ <- logger.infoUIO("Sending message: "+msg)
      _ <- outTopic.publish1(msg).flatMapError(e => logger.errorUIO("Error sending message", e))
      sleepTime = scala.util.Random.between(250, 1000)
      _ <- ZIO.sleep(zio.duration.Duration.fromScala(sleepTime.milli))
    } yield ()
    prg.forever.fork
  }

}